#General notes about the application#
 * Java version 1.8 was used
 * Maven was selected as a builder
 * Initially I created the Threaded version, but after seeing performance I decided to create a non threaded version
  * All notes here are on the threaded version except for lessons learned.
  * to run threaded use flag -t
 
#Unhandled potential edge cases#
 * double asterisk in a pattern will be treated as a literal string of \*\*
     * I thought about what to do about it, and was not really sure
       * treat it as a multi-length wild card
       * throw an error
       * treat it as a literal *

#Execution time#
 * TLDR; **O(N Log N/s)**  Process time on my computer: **15,871,200 paths in 452 seconds**
   * s is the average spread of sizes the paths have
  * There is O(N) with low probability on the pattern processing, if all patterns are a different size

###Execution time edge cases###
* Patterns passed in = 0
    * This is not expected to be a significant change in execution from specific code to handle it
    * The map lookup for the path is O(1) and O(N) for the paths regardless to print "NO MATCH"
 * If the number of paths passed in is 0
     * Technically this is handled in assumed best fashion 
       * path>0: pattern parsing would be O(N<sup>2</sup> LOG N/s) to make the short cut
       * As this is assumed a low probability it has no value

###Additional performance notes###
* Memory 
    * Factors
      * I am not maintaining values of paths once they are output
      * Output happens as soon as the results are able to be lined up
      * I do take in new paths until I am done with a thread
    * Results:
      * If input buffer is limited by the OS, the actual library will not take up as much memory
      * As long as the input buffering is managed 
          i.e. from a file which should limit the buffer, memory should stay low
      
#As a package#
 * I would potentially look at using decorators and factors to allow it to be more versatile and dynamic
 * While the threaded version was relatively viable as a library, the unthreaded one would also require the Unthreaded class to be part of the library.
 * I would create a configuration file to set many of the defaults and constants
 * Changes to maven to publish to a repo would also be required.
 * I would also need to detail the public functions better as well as compile javadocs as part of the build.
 
#Lessons Learned from threaded results vs unthreaded#

On my machine the threaded version is able to process about 16,700 paths per second in the testing environment.  This seems extremely slow to me.  The unthreaded version ran more than 4 times faster.

 The unthreaded application is much simpler, removing 2 classes as well as greatly simplifying the main processing class.  In addition to being simpler it runs faster.  Threaded took \~110 seconds to process 871200 items, while Unthreaded to \~25 seconds.  I believe this is a combination of both trying to manage synchronization, as well as the cost of creating and starting threads.  In short my assumption that multithreading would give it a slight boost was incorrect.

 I do believe that if I only create the specified threads once, then have them grab the next available item and take out synchronizations that are not actually needed that I can get the Threaded version to be as fast as the unthreaded one (possibly faster).  After thinking about the way I did the threading I believe I did it poorly.
 
 I intend to play with the mulithreading as well as experiment with different methods to handle I/O to see if I can significantly improve performance, as I suspect I may be able to get it to run as much as twice as fast.
 
I build a unit test that would run different pattern sizes.  WHen using 100000 thread max, it was only able to reach 4 threads on my machine with most of the time only running 2. After seeing the unthreaded version run, I believe my original assumption of the io being the bottleneck was incorrect and it is likely the creation of so many thread as well as synchronizing code that currently does not need it. 

As a result making the application multi-threaded was probably not worth the effort and is potentially buggy as a result.  If the processing was more complex it would be worth it, but i/o is the slowest part.  However I believe it possible to fix some inefficiencies in design and removing syncs around integer increment/decrements and get the threaded version to run faster.

If you have a chance to run the speed unit test on something that may have more ability to run faster on i/o I believe the thread usage will drastically increase and the process will be faster.

#Additional notes#

##Application arguments##
 * Arguments implemented
     * -q Quiet, suppresses standard error output
     * -s Synchronize output (wait until all input is collected before starting output)
        * Only available in threaded
     * -u Handle "." and ".." in paths
     * -t Run threaded
       * a number passed in will indicate the number of threads if t was in the previous argument
       * The default of 10 will be used.
     * -? Display help.
 * Arguments/Options I thought about doing, but did not implement
     * Error on path element that are blank i.e. a//b
     * Ignore case
     * Allow for the path separator to be escaped using standard Linux path escape \/
     * Disallow characters that are not usually a good idea, such as |, /, :, or termination character
     * Potential additional input after the number of lines expected finished
 
##Design choices##
 * In truth I probably did more in line comments than I normally would as I am not having this peer reviewed


 * There are exceptions that should be unreachable, I tend to use these to detect unexpected bugs with integration


 * As speed was identified as a target, I designed it to utilize threads to process the directories in parallel
          
   While threading adds an additional layer of complexity, it provides a boost in processing efficiency that may be worthwhile when used in big data, which was indicated in the notes provided.

 * Unit Testing
 
   I like to use real objects for unit tests as it seems to greatly help with reducing integration bugs.  As such, I tend to only mock objects for testing for cases where using real objects would make the test more complex, unreliable, or makes sense.

 
##Unit Test Coverage##
       Element                Coverage
	  All Source                92.3%
	  app.Main.java             62.4%
	  pathanalyzer directory    96.7%
	  Manager.java              89.5%
	  PathEval.java             83.7%
      AppConstants.java         76.9%
      Pattern.java              100.0%

  