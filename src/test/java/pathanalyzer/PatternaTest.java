/**
 * 
 */
package pathanalyzer;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import org.junit.jupiter.api.Test;

import pathanalyzer.exceptions.PathException;

/**
 * Named PaternaTest to ensure it shows up next to the class it tests.
 */
class PatternaTest {

	@Test
	void ConstructorAndPropertiesTest() {

		Pattern p1 = new Pattern("a,*,c,*,x");
		Pattern p2 = new Pattern(new PathException("Test Message"));

		// Test patternText
		assertTrue(p1.toString().equals("a,*,c,*,x"));
		assertTrue(p2.toString() == null);

		// Test elements expected
		String[] tElem = new String[] { "a", "*", "c", "*", "x" };
		assertEquals(5, p1.getElements().length);
		assertEquals(5, p1.getLength());
		for (int i = 0; i < p1.getLength(); i++) {
			assertTrue(p1.getElements()[i].equals(tElem[i]));
		}

		// Test wildSpots
		assertEquals(2, p1.getWildSpots().size());
		assertEquals(1, p1.getWildSpots().get(0));
		assertEquals(3, p1.getWildSpots().get(1));

		// Test exceptions
		assertTrue(p1.getException() == null);
		assertTrue(p2.getException().getMessage().equals("Test Message"));

		assertEquals(false, p1.hasError());
		assertEquals(true, p2.hasError());
	}

	@Test
	void arraySortTest() {
		List<Pattern> list = new ArrayList<Pattern>();

		list.add(new Pattern("*,b,*"));
		list.add(new Pattern("a,*,*"));
		list.add(new Pattern("*,*,c"));
		list.add(new Pattern("foo,bar,baz"));
		list.add(new Pattern("w,*,*"));
		list.add(new Pattern("*,y,z"));
		list.add(new Pattern("*,*,*"));

		Collections.sort(list);

		assertEquals("foo,bar,baz", list.get(0).toString());
		assertEquals("*,y,z", list.get(1).toString());
		assertEquals("a,*,*", list.get(2).toString());
		assertEquals("w,*,*", list.get(3).toString());
		assertEquals("*,b,*", list.get(4).toString());
		assertEquals("*,*,c", list.get(5).toString());
		assertEquals("*,*,*", list.get(6).toString());
	}

	@Test
	void testProvided() throws PathException {

		String[] patterns = new String[] { "*,b,*", "a,*,*", "*,*,c", "foo,bar,baz", "w,x,*,*", "*,x,y,z" };

		HashMap<Integer, List<Pattern>> map = Pattern.buildPatternMap(patterns);

		Pattern results = Pattern.evalPath(map, "/w/x/y/z/", false);
		assertTrue(results.toString().equals("*,x,y,z"));

		results = Pattern.evalPath(map, "a/b/c", false);
		assertTrue(results.toString().equals("a,*,*"));

		results = Pattern.evalPath(map, "foo/", false);
		assertTrue(results.toString().equals("NO MATCH"));

		results = Pattern.evalPath(map, "foo/bar/", false);
		assertTrue(results.toString().equals("NO MATCH"));

		results = Pattern.evalPath(map, "foo/bar/baz/", false);
		assertTrue(results.toString().equals("foo,bar,baz"));

	}

	@Test
	void TestBuildPatternMap() {

		StringBuilder logger = new StringBuilder("\nTestBuildPatternMap\n");

		String[] patterns = new String[] { "b,d", "a,b,c", "*,x,y,z", "a,c", "*,a,c", "*,*,a,b,c", "a,*,b,*,c,*",
				"*,a,*", "a", "*", "a,b,c,d,e,f,g", "y,z,*,a", "b,*,a,d,e", "letters,*,*", "*,*,*" };

		HashMap<Integer, List<Pattern>> map = Pattern.buildPatternMap(patterns);

		Set<Integer> keys = map.keySet();
		for (Integer key : keys) {
			logger.append("Items for size: ").append(key).append('\n');
			List<Pattern> items = map.get(key);
			for (Pattern p : items) {
				logger.append(p.toString()).append('\n');
			}

		}

		System.out.println(logger.toString());

		assertTrue(map.get(1).size() == 2);
		assertTrue(map.get(1).get(0).toString().equals("a"));
		assertTrue(map.get(1).get(1).toString().equals("*"));

		assertTrue(map.get(2).size() == 2);
		assertTrue(map.get(2).get(0).toString().equals("b,d"));
		assertTrue(map.get(2).get(1).toString().equals("a,c"));

		assertTrue(map.get(3).size() == 5);
		assertTrue(map.get(3).get(0).toString().equals("a,b,c"));
		assertTrue(map.get(3).get(1).toString().equals("*,a,c"));
		assertTrue(map.get(3).get(2).toString().equals("letters,*,*"));
		assertTrue(map.get(3).get(3).toString().equals("*,a,*"));
		assertTrue(map.get(3).get(4).toString().equals("*,*,*"));

		assertTrue(map.get(4).size() == 2);
		assertTrue(map.get(4).get(0).toString().equals("y,z,*,a"));
		assertTrue(map.get(4).get(1).toString().equals("*,x,y,z"));

		assertTrue(map.get(5).size() == 2);
		assertTrue(map.get(5).get(0).toString().equals("b,*,a,d,e"));
		assertTrue(map.get(5).get(1).toString().equals("*,*,a,b,c"));

		assertTrue(map.get(6).size() == 1);
		assertTrue(map.get(6).get(0).toString().equals("a,*,b,*,c,*"));

		assertTrue(map.get(7).size() == 1);
		assertTrue(map.get(7).get(0).toString().equals("a,b,c,d,e,f,g"));

		assertTrue(map.get(8) == null);
	}

	@Test
	void HandleUpDirTest() throws PathException {
		String[] pathElements1 = new String[] { "..", "..", ".", "a", ".", "b", "..", "c" };

		String[] results;
		results = Pattern.handleUpDir(pathElements1, false);

		assertEquals(4, results.length);
		assertTrue(results[0].equals(".."));
		assertTrue(results[1].equals(".."));
		assertTrue(results[2].equals("a"));
		assertTrue(results[3].equals("c"));

		assertThrows(PathException.class, () -> Pattern.handleUpDir(pathElements1, true),
				"Path tries to jump above root");

		// Note: This pattern makes no sense to use, however as I am handling an edge
		// case, yadda.
		String[] pathElements2 = new String[] { "a", "..", "..", "b", "c" };

		results = Pattern.handleUpDir(pathElements2, false);

		assertEquals(3, results.length);
		assertTrue(results[0].equals(".."));
		assertTrue(results[1].equals("b"));
		assertTrue(results[2].equals("c"));

		assertThrows(PathException.class, () -> Pattern.handleUpDir(pathElements2, true),
				"Path tries to jump above root");

		String[] pathElements3 = new String[] { "a", "..", "c" };

		results = Pattern.handleUpDir(pathElements3, false);

		assertEquals(1, results.length);
		assertTrue(results[0].equals("c"));

		assertDoesNotThrow(() -> {
			Pattern.handleUpDir(pathElements3, true);
		}, "Path tries to jump above root");
		results = Pattern.handleUpDir(pathElements3, true);

		assertEquals(1, results.length);
		assertTrue(results[0].equals("c"));
	}

	@Test
	void parsePathTest() throws PathException {

		String[] results;

		// Not root
		String path1 = "../a/b";

		results = Pattern.parsePath(path1, false);

		assertEquals(3, results.length);
		assertTrue(results[0].equals(".."));
		assertTrue(results[1].equals("a"));
		assertTrue(results[2].equals("b"));

		// As it is not a root directory pattern this should not impact the results.
		results = Pattern.parsePath(path1, true);

		assertEquals(3, results.length);
		assertTrue(results[0].equals(".."));
		assertTrue(results[1].equals("a"));
		assertTrue(results[2].equals("b"));

		// rooted path
		String path2 = "/../a/b/c/";

		results = Pattern.parsePath(path2, false);

		assertEquals(4, results.length);
		assertTrue(results[0].equals(".."));
		assertTrue(results[1].equals("a"));
		assertTrue(results[2].equals("b"));
		assertTrue(results[3].equals("c"));

		String path3 = "a//b";

		results = Pattern.parsePath(path3, false);

		assertEquals(3, results.length);
		assertTrue(results[0].equals("a"));
		assertTrue(results[1].equals(""));
		assertTrue(results[2].equals("b"));

		assertThrows(PathException.class, () -> Pattern.parsePath(path2, true), "Path tries to jump above root");
	}

	@Test
	void EvalPathTest() throws PathException {
		Pattern p1 = new Pattern("a,b,c");
		Pattern p2 = new Pattern("a,*,c");
		Pattern p3 = new Pattern("*,b,*");

		String[] path1 = new String[] { "a", "b", "c" };
		String[] path2 = new String[] { "a", "d", "c" };
		String[] path3 = new String[] { "a", "b", "c", "d" };

		assertEquals(true, p1.testPath(path1));
		assertEquals(false, p1.testPath(path2));
		assertThrows(PathException.class, () -> p1.testPath(path3), "Path and Pattern lengths do not match");

		assertEquals(true, p2.testPath(path1));
		assertEquals(true, p2.testPath(path2));
		assertThrows(PathException.class, () -> p2.testPath(path3), "Path and Pattern lengths do not match");

		assertEquals(true, p3.testPath(path1));
		assertEquals(false, p3.testPath(path2));
		assertThrows(PathException.class, () -> p3.testPath(path3), "Path and Pattern lengths do not match");

	}

	@Test
	void evalPathTest() throws PathException {

		String[] patterns = new String[] { "a,b,c", "*,b,c", "*,a,*" };
		HashMap<Integer, List<Pattern>> map = Pattern.buildPatternMap(patterns);

		Pattern results;

		results = Pattern.evalPath(map, "a/b/c", false);
		assertTrue(results.toString().equals("a,b,c"));

		results = Pattern.evalPath(map, "b/b/c", false);
		assertTrue(results.toString().equals("*,b,c"));

		results = Pattern.evalPath(map, "d/a/x", false);
		assertTrue(results.toString().equals("*,a,*"));

		results = Pattern.evalPath(map, "/d/a/x/", false);
		assertTrue(results.toString().equals("*,a,*"));

		results = Pattern.evalPath(map, "d/s/x", false);
		assertTrue(results.toString().equals("NO MATCH"));

		results = Pattern.evalPath(map, "//a/x", false);
		assertTrue(results.toString().equals("*,a,*"));

	}

}
