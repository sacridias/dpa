package pathanalyzer;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import pathanalyzer.exceptions.PathException;
import pathanalyzer.exceptions.ThreadingException;

class ManagerTest {

	static final String[] patterns = new String[] { "b,d", "a,b,c", "*,x,y,z", "a,c", "*,a,c", "*,*,a,b,c", "a,*,b,*,c,*",
			"*, a, *", "a", "*", "a,b,c,d,e,f,g", "y,z,*,a", "b,*,a,d,e", "letters,*,*", "*,*,*" };

	@Test
	void testManager() {
		Manager theMan = new Manager(patterns, 10, true, 0);
		Manager theWoMan = new Manager(patterns, 10, false, 0);
		assertEquals(false, theMan.isActive());
		assertEquals(false, theWoMan.isActive());
	}

	@Test
	void testFullRunNoSimplifyDir() throws InterruptedException, ThreadingException, PathException {
		Manager theMan = new Manager(patterns, 5, false, 10);
		assertEquals(true, theMan.isActive());
		theMan.addPath("a/b/c");
		theMan.addPath("a/x/c");
		theMan.addPath("a/b/c/k");
		theMan.addPath("b/d");
		theMan.addPath("//x/y/z");
		theMan.addPath("a/t/w/j");
		theMan.addPath("j/a/c");
		theMan.addPath("q/r/s/t/u");
		theMan.addPath("letters/bob/smith");
		theMan.addPath("a/x/b/t/c//");
		
		ArrayList<String> expected = new ArrayList<String>();
		expected.add("a,b,c");
		expected.add("*,*,*");
		expected.add("NO MATCH");
		expected.add("b,d");
		expected.add("*,x,y,z");
		expected.add("NO MATCH");
		expected.add("*,a,c");
		expected.add("NO MATCH");
		expected.add("letters,*,*"); //Note: Letters,*,* is a lesser match by the rules i interpreted.
		expected.add("a,*,b,*,c,*");
		
		ArrayList<String> results = new ArrayList<String>();
		int count = 0;
		while(theMan.isActive() && count < 100) {
			List<String> out = theMan.getOutput();
			if(out != null) {
				results.addAll(out);
			}
			count++;
			AppConstants.sleep();
		}
		assertTrue(count < 100);
		assertLinesMatch(expected, results);		
	}

	@Test
	void testFullRunSimplifyDir() throws InterruptedException, ThreadingException, PathException {
		Manager theMan = new Manager(patterns, 5, true, 10);
		assertEquals(true, theMan.isActive());
		theMan.addPath("a/b/c");
		theMan.addPath("../a/c");
		theMan.addPath("a/b/c/k/..");
		theMan.addPath("b/d");
		theMan.addPath("b/x/y/z");
		theMan.addPath("a/t/w/j");
		theMan.addPath("j/a/c");
		theMan.addPath("q/r/s/t/u");
		theMan.addPath("letters/bob/smith");
		theMan.addPath("a/x/b/t/c//");
		
		ArrayList<String> expected = new ArrayList<String>();
		expected.add("a,b,c");
		expected.add("*,a,c");
		expected.add("a,b,c");
		expected.add("b,d");
		expected.add("*,x,y,z");
		expected.add("NO MATCH");
		expected.add("*,a,c");
		expected.add("NO MATCH");
		expected.add("letters,*,*");
		expected.add("a,*,b,*,c,*");
		
		ArrayList<String> results = new ArrayList<String>();
		int count = 0;
		while(theMan.isActive() && count < 100) {
			List<String> out = theMan.getOutput();
			if(out != null) {
				results.addAll(out);
			}
			count++;
			AppConstants.sleep();
		}
		assertTrue(count < 100);
		assertLinesMatch(expected, results);		
	}
	
//	
//
//	assertTrue(map.get(1).size() == 2);
//	assertTrue(map.get(1).get(0).toString().equals("a"));
//	assertTrue(map.get(1).get(1).toString().equals("*"));
//
//	assertTrue(map.get(2).size() == 2);
//	assertTrue(map.get(2).get(0).toString().equals("b,d"));
//	assertTrue(map.get(2).get(1).toString().equals("a,c"));
//
//	assertTrue(map.get(3).size() == 5);
//	assertTrue(map.get(3).get(0).toString().equals("a,b,c"));
//	assertTrue(map.get(3).get(1).toString().equals("*,a,c"));
//	// TODO: Not certain these next cases are correct. As I read the document this
//	// is how I interpreted it though.
//	// Specific concern is the second case would never be reached.
//	assertTrue(map.get(3).get(2).toString().equals("*,*,*"));
//	assertTrue(map.get(3).get(3).toString().equals("*,a,*"));
//	assertTrue(map.get(3).get(4).toString().equals("letters,*,*"));
//
//	assertTrue(map.get(4).size() == 2);
//	assertTrue(map.get(4).get(0).toString().equals("*,x,y,z"));
//	assertTrue(map.get(4).get(1).toString().equals("y,z,*,a"));
//
//	assertTrue(map.get(5).size() == 2);
//	assertTrue(map.get(5).get(0).toString().equals("*,*,a,b,c"));
//	assertTrue(map.get(5).get(1).toString().equals("b,*,a,d,e"));
//
//	assertTrue(map.get(6).size() == 1);
//	assertTrue(map.get(6).get(0).toString().equals("a,*,b,*,c,*"));
//
//	assertTrue(map.get(7).size() == 1);
//	assertTrue(map.get(7).get(0).toString().equals("a,b,c,d,e,f,g"));
//
//	assertTrue(map.get(8) == null);
//		
//
//		/**
//		 * This function takes a new path and creates a worker to deal with it.
//		 * 
//		 * @param path
//		 * @throws InterruptedException
//		 */
//		public void addPath(String path) throws InterruptedException {
//			while (this.getActiveThreads() >= maxThreads) {
//				Thread.sleep(0, AppConstants.threadSleepTime);
//			}
//			// While not needed now, I find it is safer to synchronize in case the process
//			// gets complicated and it is missed.
//			synchronized (activeThreads) {
//				activeThreads++;
//			}
//			// Pass in all needed parameters to worker and start it. Not this is passed in
//			// to allow access to post results.
//			PathEval thread = new PathEval(path, ++inputIndex, this);
//			thread.start();
//		}
//
//		/**
//		 * This function will capture the results, it is expected to only be called by
//		 * eval threads.
//		 */
//		protected void postResults(int index, Pattern results) {
//			pathMap.put(index, results);
//			synchronized (activeThreads) {
//				activeThreads--;
//			}
//		}
//
//		/**
//		 * Used to check active thread count
//		 * 
//		 * @return
//		 */
//		private Integer getActiveThreads() {
//			// While not needed now, I find it is safer to synchronize in case the process
//			// gets complicated and it is missed.
//			synchronized (activeThreads) {
//				return activeThreads;
//			}
//		}
//
//		/**
//		 * Ensures output order matches input order and returns any available output.
//		 * 
//		 * @return
//		 * @throws ThreadingException
//		 * @throws PathException
//		 */
//		public List<String> getOutput() throws ThreadingException, PathException {
//			if (outputIndex >= totalItems)
//				return null; // All output has already been captured
//			List<String> output = new ArrayList<String>();
//			while (pathMap.containsKey(outputIndex + 1)) {
//				outputIndex++;
//				if (pathMap.get(outputIndex).hasError()) {
//					throw pathMap.get(outputIndex).getException();
//				}
//				String s = pathMap.get(outputIndex).toString();
//				if (s == null) {
//					throw new ThreadingException("Thread posted without setting match");
//				}
//				output.add(pathMap.get(++outputIndex).toString());
//				// Clean up the object to free up memory
//				pathMap.remove(outputIndex);
//			}
//			return output;
//		}
//
//		/**
//		 * Returns false once all items are processed and collected
//		 * 
//		 * @return
//		 */
//		public boolean isActive() {
//			if (getActiveThreads() > 0)
//				return true;
//			else if (totalItems > outputIndex)
//				return true;
//			else
//				return false;
//		}
//
//		/*
//		 * Get flag value of simplifyUpDir
//		 */
//		public Boolean getSimplifyUpDir() {
//			return this.simplifyUpDir;
//		}

}
