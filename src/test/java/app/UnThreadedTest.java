package app;

import static org.junit.jupiter.api.Assertions.*;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;

import org.junit.jupiter.api.Test;

import pathanalyzer.exceptions.PathException;

/**
 * While I am running these as unit tests, they are actually full run integration tests.
 *
 */
class UnThreadedTest {

	@Test
	void testProvided() throws PathException, IOException {
		InputStream in = new FileInputStream(new File("src/test/resources/TestInput.provided"));
		ByteArrayOutputStream out = new ByteArrayOutputStream();

		UnThreaded app = new UnThreaded(in, new PrintStream(out), false);
		app.process();

		String[] results = out.toString().split("\n");
			
		assertTrue(results[0].trim().equals("*,x,y,z"));
		assertTrue(results[1].trim().equals("a,*,*"));
		assertTrue(results[2].trim().equals("NO MATCH"));
		assertTrue(results[3].trim().equals("NO MATCH"));
		assertTrue(results[4].trim().equals("foo,bar,baz"));

	}

	void largeDataProcess() throws InterruptedException, IOException, PathException {
		InputStream in = new FileInputStream(new File("src/test/resources/TestInput.largerData"));
		ByteArrayOutputStream out = new ByteArrayOutputStream();

		UnThreaded app = new UnThreaded(in, new PrintStream(out), false);
		Thread t = new Thread() {
			@Override
			public void run() {
				while(app.getProgressPerc() != 100) {
					System.out.println(app.getProgress());
					out.reset();
					try {
						Thread.sleep(1000 *10);
					} catch (InterruptedException e) {
						System.err.println("Unexpected interruption of monitor thead");
					}
				}
			}
		};
		t.start();
		app.process();
	}

	/**
	 * The file specified has 15871200 paths to evaluate.
	 * 
	 * @throws InterruptedException
	 * @throws IOException
	 * @throws PathException 
 	 * Test is disabled to make testing faster
	 * It is suggested to edit the big data file to reduce from 15,000,000 records to a lower number as this would take ~ 1/2 hour to run.

	 */
//	@Test
	void testLargeData() throws InterruptedException, IOException, PathException {

		long start = System.nanoTime();
		largeDataProcess();
		long endDur = System.nanoTime() - start;

		System.out.println(
				String.format("End time 1: %d seconds and %d nanoseconds", endDur / 1000000000, endDur % 1000000000));
	}

	@Test
	void testMisAlignedInput() throws FileNotFoundException {
		InputStream in = new FileInputStream(new File("src/test/resources/TestInput.misalignedInput"));
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		
		UnThreaded app = new UnThreaded(in, new PrintStream(out), false);
		assertThrows(NumberFormatException.class, () -> app.process());
	}
}
