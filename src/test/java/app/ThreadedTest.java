package app;

import static org.junit.jupiter.api.Assertions.*;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;

import org.junit.jupiter.api.Test;

import pathanalyzer.exceptions.PathException;
import pathanalyzer.exceptions.ThreadingException;

/**
 * While I am running these as unit tests, they are actually full run integration tests.
 *
 */
class ThreadedTest {

	@Test
	void testProvided() throws NumberFormatException, IOException, InterruptedException, ThreadingException, PathException {
		InputStream in = new FileInputStream(new File("src/test/resources/TestInput.provided"));
		ByteArrayOutputStream out = new ByteArrayOutputStream();

		Threaded app = new Threaded(in, new PrintStream(out), 10, false, false);
		app.process();

		String[] results = out.toString().split("\n");
		assertTrue(results[0].equals("*,x,y,z"));
		assertTrue(results[1].equals("a,*,*"));
		assertTrue(results[2].equals("NO MATCH"));
		assertTrue(results[3].equals("NO MATCH"));
		assertTrue(results[4].equals("foo,bar,baz"));

	}

	void largeDataProcess(Integer threadSize) throws InterruptedException, IOException {
		InputStream in = new FileInputStream(new File("src/test/resources/TestInput.largerData"));
		ByteArrayOutputStream out = new ByteArrayOutputStream();

		Threaded app = new Threaded(in, new PrintStream(out), 10, false, false);

		Thread t = new Thread() {
			@Override
			public void run() {
				try {
					app.process();
				} catch (NumberFormatException | IOException | InterruptedException | ThreadingException
						| PathException e) {
					System.err.println("Exception in executions");
				}
			}
		};
		t.start();
		while (t.isAlive()) {
			System.out.println(app.getProgress());
			System.out.println(String.format("Threads: %d of %d used", app.getActiveThreads(), threadSize));

			int size1 = out.size();
//			out.toByteArray(); // Clear buffer
			out.reset();
			int size2 = out.size();
			if (size1 != 0 && size2 >= size1) {
				System.out.println("Output buffer not cleared");
			}
			Thread.sleep(1000 * 10);
		}
		if(app.getProgressPerc() != 100) throw new IOException("Something afoot the circle k");
	}

	/**
	 * The file specified has 15871200 paths to evaluate.
	 * 
	 * @throws InterruptedException
	 * @throws IOException 
	 * 
	 * Test is disabled to make testing faster
	 * It is suggested to edit the big data file to reduce from 15,000,000 records to a lower number as this would take ~ 1/2 hour to run.
	 */
//	@Test
	void testLargeData() throws InterruptedException, IOException {

		long start5 = System.nanoTime();
		largeDataProcess(100000);
		long endDur5 = System.nanoTime() - start5;

		System.out.println(String.format("End time 1: %d seconds and %d nanoseconds", endDur5/1000000000, endDur5 % 1000000000));

//		long start1 = System.nanoTime();
//		largeDataProcess(10);
//		long endDur1 = System.nanoTime() - start1;

//		System.out.println(String.format("End time 1: %d seconds and %d nanoseconds", endDur1/1000000000, endDur1 % 1000000000));

//		long start2 = System.nanoTime();
//		largeDataProcess(100);
//		long endDur2 = System.nanoTime() - start2;

//		System.out.println(String.format("End time 1: %d seconds and %d nanoseconds", endDur2/1000000000, endDur2 % 1000000000));

//		long start3 = System.nanoTime();
//		largeDataProcess(10);
//		long endDur3 = System.nanoTime() - start3;
//
//		System.out.println(String.format("End time 1: %d seconds and %d nanoseconds", endDur3/1000000000, endDur3 % 1000000000));

//		long start4 = System.nanoTime();
//		largeDataProcess(10000);
//		long endDur4 = System.nanoTime() - start4;

//		System.out.println(String.format("End time 1: %d", endDur4/1000000000));

	}

	@Test
	void testMisAlignedInput() throws FileNotFoundException {
		InputStream in = new FileInputStream(new File("src/test/resources/TestInput.misalignedInput"));
		ByteArrayOutputStream out = new ByteArrayOutputStream();

		Threaded app = new Threaded(in, new PrintStream(out), 10, false, false);

		assertThrows(NumberFormatException.class, () -> app.process());

	}
}
