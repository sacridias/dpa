package app;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.text.DecimalFormat;
import java.util.List;

import pathanalyzer.Manager;
import pathanalyzer.exceptions.PathException;
import pathanalyzer.exceptions.ThreadingException;
import pathanalyzer.AppConstants;

/**
 * This class is the original class to handled Threaded Processing
 */
public class Threaded {
	static final DecimalFormat PERCFORMAT = new DecimalFormat("#.00");
	
	/**
	 * 
	 * Class Variables
	 * 
	 */
	Boolean simplifyUpDir;
	// -q Suppress output to standard error
	Boolean quiet;
	// -s Ensure output is not started until all input is collected.
	Boolean syncInOut;

	//Number of threads to allow
	Integer threadLimit;

	// Designates that input termination character was detected.
	Boolean endOfInput = false;
	
	//The manager used
	Manager manager = null; 

	// Variables to handle input and output to make it more testable.
	InputStream input;
	PrintStream output;

	//Allows you to inspect progress. 
	Integer pathCount = 0;
	int handledPaths = 0;
	

	/**
	 * Basic constructor
	 * @param in
	 * @param out
	 * @param threadLimit
	 * @param simplifyUpDir
	 * @param syncInOut
	 */
	public Threaded(
			InputStream in, PrintStream out,
			int threadLimit, 
			Boolean simplifyUpDir, Boolean syncInOut
	) {
		this.input = in;
		this.output = out;
		this.threadLimit = threadLimit;
		this.simplifyUpDir = simplifyUpDir;
		this.syncInOut = syncInOut;
	}

	/**
	 * This function coordinates standard input and output as well as configuring
	 * and loading the analysis manager.
	 * 
	 * @throws NumberFormatException
	 * @throws IOException
	 * @throws InterruptedException
	 * @throws ThreadingException
	 * @throws PathException
	 */
	public void process()
			throws NumberFormatException, IOException, InterruptedException, ThreadingException, PathException {

		String[] patterns = getPatterns();
		
		try {
			pathCount = Integer.parseInt(getInput());
		}
		// Manage Exception message to be meaningful.
		catch (NumberFormatException e) {
			NumberFormatException ne = new NumberFormatException("Pattern count expected as first parameter");
			ne.addSuppressed(e);
			throw ne;
		}

		this.manager = new Manager(patterns, threadLimit, this.simplifyUpDir, pathCount);

		// The Manager will task threads to process these elements.
		for (int i = 0; i < pathCount; i++) {

			manager.addPath(getInput());
			// If syncInOut is set, do not collect and post results until all input is read.
			if (!this.syncInOut) {

				handleOutPut();
			}
		}
		// Continue to write out results until the manager has collected all results.
		while (manager.isActive()) {
			AppConstants.sleep();

			handleOutPut();
		}
	}

	/**
	 * Reads from the manager for any results, and prints it out.
	 * 
	 * @throws ThreadingException
	 * @throws PathException
	 */
	private void handleOutPut() throws ThreadingException, PathException {

		List<String> outputList = manager.getOutput();
		if (outputList != null) {
			for (String s : outputList) {
				handledPaths++;
				output.print(s + "\n");
			}
		}
	}

	/**
	 * get the first line as count Each additional line is expected as a pattern to
	 * the number specified by count.
	 * 
	 * @return An array of patterns.
	 * @throws NumberFormatException
	 * @throws IOException
	 */
	private String[] getPatterns() throws NumberFormatException, IOException {
		try {
			Integer count = Integer.parseInt(getInput());
			String[] patterns = new String[count];
			for (int i = 0; i < count; i++) {

				patterns[i] = this.getInput();

				if (patterns[i].equals("")) {
					// I can think of no reason the pattern should be empty.
					throw new IOException(String.format(
							"Expected pattern %d of %d but input line was empty or terminator was reached", i, count));
				}
			}
			return patterns;
		}
		// Manage Exception message to be meaningful to input expectations
		catch (NumberFormatException e) {
			NumberFormatException ne = new NumberFormatException("Pattern count expected as first parameter");
			ne.addSuppressed(e);
			throw ne;
		}
	}

	/**
	 * This method gets the next line and returns it. It will also return it if it
	 * encounters end of file. In case the last line does not have \n in it.
	 * 
	 * @return
	 * @throws IOException
	 */
	private String getInput() throws IOException {
		if (this.endOfInput) {
			// Instead of using end of file exception, this allows greater control over the
			// message.
			throw new IOException("Requesting input after end of input detected");
		}

		String line = "";
		int ch;
		// Look for end of file
		while ((ch = input.read()) != -1) {
			if (ch == '\r')
				continue; // Just in case it somehow got in there.
			if (ch == '\n')
				return line;
			line += (char) ch;
		}
		// Indicate terminator was reached to disable further attempts to get input.
		endOfInput = true;
		return line;
	}
	
	public String getProgress() {
		Float perc = ((new Float(this.handledPaths)/new Float(this.pathCount)) *100);
		StringBuilder results = new StringBuilder("");
		results
			.append("Process has completed ")
			.append(this.handledPaths)
			.append(" of ")
			.append(this.pathCount)
			.append(" Paths: ")		    
			.append(PERCFORMAT.format(perc))
			.append("% complete");
		return results.toString();
	}

	public Integer getActiveThreads() {
		if(this.manager != null) {
			return manager.getActiveThreads();
		}
		return -1;
	}

	public float getProgressPerc() {
		return ((new Float(this.handledPaths)/new Float(this.pathCount)) *100);
	}

}
