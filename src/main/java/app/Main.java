package app;

import java.io.IOException;
import java.util.Properties;

import pathanalyzer.exceptions.PathException;
import pathanalyzer.exceptions.ThreadingException;

/**
 * Please read Notes.md in base directory for more information.
 * 
 * This class acts as an application to handle input and output Please use -?,
 * look at class boolean comments, notes, or processArgs function to see
 * parameters. Note: The Notes.md file is a list of options I thought about, but
 * decided not to get into, as I wanted to ensure the project was not too
 * massive when unneeded.
 */
public class Main {
	
	private static final Integer DEFAULT_THREAD_LIMIT = 10;
	
	private static boolean quiet = false;

	/**
	 * Entry Point Creates a self class to assist with testing passing in input and
	 * output for testing and dynamic capabilities.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		try {
			Properties props = processArgs(args);
			if(props.contains("threaded")) {
				runThreaded(props);
			}
			else {
				runUnthreaded(props);
			}
		} catch (NumberFormatException | IOException | PathException e) {

			processError(e);
			if (!quiet) {
				System.err.println("Premature termination due to last error");
			}
		} catch (InterruptedException | ThreadingException e) {

			processError(e);
			if (!quiet) {
				System.err.println("Application termination detected, results not complete");
			}
		}
	}
	
	private static void runUnthreaded(Properties props) throws PathException, IOException {
		Boolean updir = props.contains("updir");
		if(props.contains("syncio")) {
			System.err.println("(WARNING): Synchronized output is not available in unthreaded mode");
		}
		
		new UnThreaded(System.in, System.out, updir).process();
		
	}


	private static void runThreaded(Properties props) throws NumberFormatException, IOException, InterruptedException, ThreadingException, PathException {
		Boolean updir = props.contains("updir");
		Boolean syncIO = props.contains("syncio");
		Integer threads = props.contains("threads")? Integer.parseInt(props.getProperty("threads")): DEFAULT_THREAD_LIMIT;
		
		new Threaded(System.in, System.out, threads, updir, syncIO).process();
			
	}

	


	/**
	 * This handles any arguments passed in during execution.
	 * 
	 * @param args
	 */
	private static Properties processArgs(String[] args) {
		
		Properties props = new Properties();

		// loop through each argument
		for (String arg : args) {
			// Ensure the first character is a -
			if (arg.charAt(0) == '-') {
				// Process the arguments.
				for (Integer i = 1; i < arg.length(); i++) {
					switch (arg.charAt(i)) {
					case 'q':
						// Prevent output to Standard Error
						quiet = true;
						break; 
					case 'u':
						// Specify to handle up directory
						props.setProperty("updir", "1");
						break;
					// Ensure input is finished before starting output response.
					case 's':
						props.setProperty("syncio", "1");
						break;
					case '?':
						printInstructions();
						return null;
					case 't':
						props.setProperty("threaded", "1");
						break;
					default:
						processError(new Exception("Error: (ignored flag) Unknown flag " + arg.charAt(i)));
						break;
					}
				}
				
			}
			else if(props.containsKey("threaded")){
				try {
					Integer.parseInt(arg);
					props.setProperty("threads", arg);
				}
				catch(Throwable t) {
					processError(new Exception("Error: (ignored) expected a flag indicator -, use -? for options"));					
				}
			}
			// No flag indicator
			else {
				processError(new Exception("Error: (ignored) expected a flag indicator -, use -? for options"));
			}
		}
		return props;
	}

	/**
	 * This function will print a list of parameters.
	 */
	private static void printInstructions() {
		StringBuilder inst = new StringBuilder();
		inst
			.append("This application has assumed functionality, with some flags for potential edge cases").append('\n')
			.append('\n')
			.append("	-q	(quiet) this will supress standard error output").append('\n')
			.append("	-s	Synchronized output to not start until input is complete").append('\n')
			.append("			Only available for threaded").append('\n')
			.append("	-t	[#]  Specifies to run threaded.  If no number is provided it will default to 10").append('\n')
			.append("	-u	Will handle .. and . in path").append('\n')
			.append("	").append('\n')
			.append("	-? If you are reading this, I assume I don't have to `splain it to you Lucy.").append('\n')
			.append("	").append('\n');
			
	}

	/**
	 * Prints out an error to standard err unless the quiet flag is set. Potential
	 * to add logging or other features.
	 * 
	 * @param e
	 */
	private static void processError(Exception e) {
		if (quiet )
			return;
		System.out.println(e.getMessage());
	}
}
