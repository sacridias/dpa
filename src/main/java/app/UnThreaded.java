package app;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;

import pathanalyzer.exceptions.PathException;
import pathanalyzer.Pattern;

/**
 * Please read Notes.md in base directory for more information.
 * 
 * This class acts as an application to handle input and output Please use -?,
 * look at class boolean comments, notes, or processArgs function to see
 * parameters. Note: The Notes.md file is a list of options I thought about, but
 * decided not to get into, as I wanted to ensure the project was not too
 * massive when unneeded.
 */
public class UnThreaded {

	static final DecimalFormat PERCFORMAT = new DecimalFormat("#.00");
	

	/**
	 * 
	 * Class Variables
	 * 
	 */
	Boolean simplifyUpDir;
	
	// Designates that input termination character was detected.
	Boolean endOfInput = false;

	// Variables to handle input and output to make it more testable.
	InputStream input;
	PrintStream output;

	//Allows you to inspect progress. 
	Integer pathCount = 0;
	int handledPaths = 0;
	
	// This is used to house the patternMap
	HashMap<Integer, List<Pattern>> patternMap;

	
	/**
	 * Standard Constructor
	 * @param in
	 * @param out
	 * @param updir
	 */
	public UnThreaded(InputStream in, PrintStream out, Boolean updir) {
		this.input = in;
		this.output = out;
		this.simplifyUpDir = updir;
	}


	/**
	 * This function controls the process.
	 * @throws IOException 
	 * @throws PathException 
	 */
	public void process() throws PathException, IOException {

		//Build the pattern map to process.
		buildPatternMap();
		
		try {
			pathCount = Integer.parseInt(getInput());
		}
		// Manage Exception message to be meaningful.
		catch (NumberFormatException e) {
			NumberFormatException ne = new NumberFormatException("Pattern count expected as first parameter");
			ne.addSuppressed(e);
			throw ne;
		}

		for (int i = 0; i < pathCount; i++) {
			Pattern results = Pattern.evalPath(patternMap, getInput(), simplifyUpDir);
			if(results.hasError()) {
				throw results.getException();
			}
			output.println(results.toString());	
			this.handledPaths++;
		}		
	}
	
	/**
	 * get the first line as count Each additional line is expected as a pattern to
	 * the number specified by count.  After getting input get the map build.
	 * @throws NumberFormatException
	 * @throws IOException
	 */
	private void buildPatternMap() throws NumberFormatException, IOException {
		try {
			Integer count = Integer.parseInt(getInput());
			String[] patterns = new String[count];
			for (int i = 0; i < count; i++) {

				patterns[i] = this.getInput();

				if (patterns[i].equals("")) {
					// I can think of no reason the pattern should be empty.
					throw new IOException(String.format(
							"Expected pattern %d of %d but input line was empty or terminator was reached", i, count));
				}
			}
			patternMap = Pattern.buildPatternMap(patterns);

		}
		// Manage Exception message to be meaningful to input expectations
		catch (NumberFormatException e) {
			NumberFormatException ne = new NumberFormatException("Pattern count expected as first parameter");
			ne.addSuppressed(e);
			throw ne;
		}
	}

	/**
	 * This method gets the next line and returns it. It will also return it if it
	 * encounters end of file. In case the last line does not have \n in it.
	 * 
	 * @return
	 * @throws IOException
	 */
	private String getInput() throws IOException {
		if (this.endOfInput) {
			// Instead of using end of file exception, this allows greater control over the
			// message.
			throw new IOException("Requesting input after end of input detected");
		}

		String line = "";
		int ch;
		// Look for end of file
		while ((ch = input.read()) != -1) {
			if (ch == '\r')
				continue; // Just in case it somehow got in there.
			if (ch == '\n')
				return line;
			line += (char) ch;
		}
		// Indicate terminator was reached to disable further attempts to get input.
		endOfInput = true;
		return line;
	}
	
	public String getProgress() {
		Float perc = getProgressPerc();
		StringBuilder results = new StringBuilder("");
		results
			.append("Process has completed ")
			.append(this.handledPaths)
			.append(" of ")
			.append(this.pathCount)
			.append(" Paths: ")		    
			.append(PERCFORMAT.format(perc))
			.append("% complete");
		return results.toString();
	}


	public float getProgressPerc() {
		return ((new Float(this.handledPaths)/new Float(this.pathCount)) *100);
	}
}
