package pathanalyzer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import pathanalyzer.exceptions.PathException;

/**
 * This class handles all pattern parsing, path parsing and testing. static
 * functions are here to keep the related code in the same spot and make it
 * easier to follow.
 */
public class Pattern implements Comparable<Pattern> {

	// This is the input text of the pattern
	private final String patternText;

	// List of pattern elements
	private String[] elements;

	// This is used to generate a small performance boost in sorting so to avoid
	// recalculating wild positions.
	private List<Integer> wildSpots;

	private PathException exception = null;

	/**
	 * constructor, builds elements
	 * 
	 * @param pattern
	 */
	public Pattern(String pattern) {
		this.patternText = pattern;
		elements = pattern.split(",", -1);
		for (int i = 0; i < elements.length; i++) {
			elements[i] = elements[i].trim();
		}
		evalWildSpots();
	}

	/**
	 * This is used if there is an error in the matching process
	 * 
	 * @param e
	 */
	public Pattern(PathException e) {
		this.patternText = null;
		this.exception = e;
	}

	/**
	 * Get elements
	 * 
	 * @return
	 */
	String[] getElements() {
		return this.elements;
	}

	/**
	 * returns the wild spot array
	 * 
	 * @return
	 */
	public List<Integer> getWildSpots() {
		return this.wildSpots;
	}

	/**
	 * This returns the number of elements in the pattern.
	 * 
	 * @return
	 */
	int getLength() {
		return elements.length;
	}

	/**
	 * Returns the exception or null if there is not one
	 * 
	 * @return
	 */
	public PathException getException() {
		return this.exception;
	}

	/**
	 * returns true/false if there was an error.
	 * 
	 * @return
	 */
	public Boolean hasError() {
		return this.exception != null;
	}

	/**
	 * Gets a list of element indexes that wild cards are used.
	 * 
	 * @return
	 */
	void evalWildSpots() {
		this.wildSpots = new ArrayList<Integer>();
		for (int i = 0; i < this.elements.length; i++) {
			if (this.elements[i].equals("*")) {
				this.wildSpots.add(i);
			}
		}
	}

	/**
	 * returns the pattern text If there was an error it will return null
	 */
	@Override
	public String toString() {
		return this.patternText;
	}

	/**
	 * This function tests the path against the pattern, if they match it returns
	 * true, otherwise it returns false.
	 * 
	 * @param pathElements
	 * @return
	 * @throws PathException
	 */
	Boolean testPath(String[] pathElements) throws PathException {
		if (pathElements.length != this.elements.length) {
			// This function assumes the lengths are the same by the mapping, this is to
			// verify that case.
			throw new PathException("Path and Pattern lengths do not match");
		}
		for (int i = 0; i < pathElements.length; i++) {
			if (!elements[i].equals("*") && !elements[i].equals(pathElements[i])) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Used to identify the rank matching of the elements. Note: sorting if < 0 then
	 * p1 is the lest ranked (better), 0 means they are equal and > 0 means p2 is
	 * the better one.
	 * 
	 */
	@Override
	public int compareTo(Pattern o) {

		// Variables to make it more legible.
		List<Integer> wild1 = this.getWildSpots();
		List<Integer> wild2 = o.getWildSpots();
		Integer size1 = wild1.size();
		Integer size2 = wild2.size();

		if (size1 == 0 && size2 == 0) {
			return 0; // Equally ranked
		}
		// If one has a size of 0, it means it has no wild cards and is thus ranked
		// better.
		if (size1 == 0)
			return -1;
		if (size2 == 0)
			return 1;

		int sizeDelta = size1.compareTo(size2);
		if (sizeDelta != 0)
			return sizeDelta; // Fewest wildcards appear first.

		// Using size1 as they are both the same at this point.
		for (int i = 0; i < size1; i++) {
			// The wildcard with the highest position wins.
			int value = wild2.get(i).compareTo(wild1.get(i));
			if (value != 0) {
				return value;
			}
		}
		// The two are equal
		return 0;
	}

	/***
	 * Static Functions
	 */

	/**
	 * This function analyzes patterns to order them for efficient evaluation. It
	 * splits the patterns into a map of lists by size Each list is then optimized
	 * for best match first Optimization that may be worth implementing: Override
	 * the sort to use parallel sorting. Implementing this would require the code
	 * become slightly more complicated I am making the assumption the pattern list
	 * is relatively small and this optimization would not be of great value and is
	 * premature.
	 * 
	 * @param patterns
	 * @return
	 */
	public static HashMap<Integer, List<Pattern>> buildPatternMap(String[] patterns) {
		HashMap<Integer, List<Pattern>> map = new HashMap<Integer, List<Pattern>>();
		Pattern[] patternList = new Pattern[patterns.length];
		for (int i = 0; i < patterns.length; i++) {
			patternList[i] = new Pattern(patterns[i]);
		}
		List<List<Pattern>> allLists = new ArrayList<List<Pattern>>();
		// Map patterns by size
		for (int i = 0; i < patternList.length; i++) {
			int size = patternList[i].getLength();
			List<Pattern> list = map.get(size);
			if (list == null) {
				list = new ArrayList<Pattern>();
				map.put(size, list);
				allLists.add(list);
			}
			list.add(patternList[i]);
		}

		// Get a list of arrays in the map and sort them by best match ranking.
		Set<Integer> keys = map.keySet();
		for (Integer key : keys) {
			Collections.sort(map.get(key));
		}

		return map;
	}

	/**
	 * This function tests the path against the patterns. Big-O = O(N log N/s) where
	 * s is the spread of sizes. There is a slight probability of O(1)
	 * 
	 * @param patterns
	 * @param path
	 * @return
	 * @throws PathException
	 */
	public static Pattern evalPath(HashMap<Integer, List<Pattern>> patternMap, String path, Boolean simplifyUpDir)
			throws PathException {
		String[] elements = parsePath(path, simplifyUpDir);

		List<Pattern> patterns = patternMap.get(elements.length);
		// No patterns match the size of the pattern.
		if (patterns == null) {
			return AppConstants.matchFailed;
		}
		for (Pattern p : patterns) {
			if (p.testPath(elements)) {
				return p;
			}
		}
		// No valid match found.
		return AppConstants.matchFailed;
	}

	/**
	 * This function parses the path into an element list.
	 * 
	 * @param path
	 * @param simplifyUpDir
	 * @return
	 * @throws PathException
	 */
	static String[] parsePath(String path, Boolean simplifyUpDir) throws PathException {
		// Strip first / if at start
		Boolean startRoot = false;
		if (path.charAt(0) == '/') {
			path = path.substring(1);
			startRoot = true;
		}
		// String trailing / if at end.
		if (path.charAt(path.length() - 1) == '/') {
			path = path.substring(0, path.length() - 1);
		}
		String[] elements = path.split("/", -1);

		if (simplifyUpDir) {
			elements = handleUpDir(elements, startRoot);
		}

		return elements;
	}

	/**
	 * This function searches for .. and . .. moves it up a directory (unless at the
	 * start) . is ignored.
	 * 
	 * @param elements
	 * @param startRoot
	 * @return
	 * @throws PathException
	 */
	static String[] handleUpDir(String[] elements, Boolean startRoot) throws PathException {
		String[] newElements = new String[elements.length];
		Integer index = 0;
		for (int i = 0; i < elements.length; i++) {
			// skip "current path"
			if (elements[i].equals("."))
				continue;
			// eval parent path for ..
			if (elements[i].equals("..")) {
				// At start or behind starting .. so handle normally.
				if (index > 0 && !newElements[index - 1].equals("..")) {
					index--;
					continue;
				}
				if (startRoot) {
					throw new PathException("Path tries to jump above root");
				}
			}
			newElements[index++] = elements[i];
		}
		elements = new String[index];
		for (int i = 0; i < index; i++) {
			elements[i] = newElements[i];
		}
		return elements;
	}

}
