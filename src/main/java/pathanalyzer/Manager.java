package pathanalyzer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import pathanalyzer.exceptions.PathException;
import pathanalyzer.exceptions.ThreadingException;

/**
 * The manager manages coordinating threads and re-ordering the results to
 * ensure they come out correctly.
 */
public class Manager {

	// map used to align output to input.
	ConcurrentHashMap<Integer, Pattern> pathMap = new ConcurrentHashMap<Integer, Pattern>();

	// These variables keep track of current index of input and output
	Integer inputIndex = 0, outputIndex = 0, totalItems;

	// Flags
	Boolean simplifyUpDir;

	// Active threads and size management
	Integer activeThreads = 0, maxThreads;

	// This is used to house the patternMap
	HashMap<Integer, List<Pattern>> patternMap;

	/**
	 * Standard Constructor
	 * 
	 * @param patterns
	 * @param threadLimit
	 */
	public Manager(String[] patterns, int threadLimit, Boolean simplifyUpDir, int count) {
		this.simplifyUpDir = simplifyUpDir;
		this.maxThreads = threadLimit;
		this.patternMap = Pattern.buildPatternMap(patterns);
		this.totalItems = count;
	}

	/**
	 * This function takes a new path and creates a worker to deal with it.
	 * 
	 * @param path
	 * @throws InterruptedException
	 */
	public void addPath(String path) throws InterruptedException {

		// Wait for threads to be under maximum.
		while (this.getActiveThreads() >= maxThreads) {
			Thread.sleep(0, AppConstants.threadSleepTime);
		}

		// While an integer does not need to be synched, I believe it is safer
		// This prevents hard to find issues if the code becomes more complex
		synchronized (activeThreads) {
			activeThreads++;
		}
		// Pass the path, index and self to the thread
		//self is used for posting which also indicates the thread is done.
		PathEval thread = new PathEval(path, ++inputIndex, this);
		thread.start();
	}

	/**
	 * This function will capture the results
	 * it is expected to only be called by workers.
	 */
	protected void postResults(int index, Pattern results) {
		
		pathMap.put(index, results);
		synchronized (activeThreads) {
			activeThreads--;
		}
	}

	/**
	 * Used to check active thread count
	 * 
	 * @return
	 */
	public Integer getActiveThreads() {
		synchronized (activeThreads) {
			return activeThreads;
		}
	}

	/**
	 * Ensures output order matches input order and returns any available output.
	 * 
	 * @return
	 * @throws ThreadingException
	 * @throws PathException
	 */
	public List<String> getOutput() throws ThreadingException, PathException {
		if (outputIndex >= totalItems)
			return null; // All output has already been captured
		List<String> output = new ArrayList<String>();

		// Check for next item
		while (pathMap.containsKey(outputIndex + 1)) {
			outputIndex++;
			if (pathMap.get(outputIndex).hasError()) {
				throw pathMap.get(outputIndex).getException();
			}
			String s = pathMap.get(outputIndex).toString();
			if (s == null) {
				throw new ThreadingException("Thread posted without setting match");
			}
			Pattern op = pathMap.get(outputIndex);
			if (op == null) {
				throw new PathException("Pattern was not expected to be null here");
			}
			String ops = op.toString();
			if (ops == null) {
				throw new PathException("Pattern string was not expected to be null here");
			}
			output.add(op.toString());

			// Clean up the object to free up memory
			pathMap.remove(outputIndex);
		}
		return output;
	}

	/**
	 * Returns false once all items are processed and collected
	 * 
	 * @return
	 */
	public boolean isActive() {
		if (getActiveThreads() > 0)
			return true;
		else if (outputIndex < totalItems)
			return true;
		else
			return false;
	}

	/*
	 * Get flag value of simplifyUpDir
	 */
	public Boolean getSimplifyUpDir() {
		return this.simplifyUpDir;
	}

}