package pathanalyzer;

import pathanalyzer.exceptions.PathException;

/**
 * This class creates a threaded object to manage the thread process to eval a
 * specific path
 *
 */
public class PathEval extends Thread {

	// Keeps track of the manager for posting results and notify it of a thread
	// finished
	Manager manager;

	// The path to be evaluated
	String path;

	// The index of this path in the list
	Integer index;

	// The pattern that was matched
	Pattern match = null;

	/**
	 * Creates a new thread to process a specific path.
	 * 
	 * @param path
	 * @param inputIndex
	 * @param manager
	 */
	public PathEval(String path, Integer inputIndex, Manager manager) {
		this.manager = manager;
		this.path = path;
		this.index = inputIndex;
	}

	/**
	 * Process the path and post results.
	 */
	@Override
	public void run() {
		try {
			try {
				this.match = Pattern.evalPath(manager.patternMap, path, manager.getSimplifyUpDir());
			} catch (PathException e) {
				this.match = new Pattern(e);
			}
			manager.postResults(this.index, this.match);
		} catch(Throwable t) {
			manager.postResults(index, AppConstants.interruptPattern); 
			throw t;
		}
	}
	
	@Override
	public boolean isInterrupted() {
		manager.postResults(index, AppConstants.interruptPattern); 
		return super.isInterrupted();
	}

}
