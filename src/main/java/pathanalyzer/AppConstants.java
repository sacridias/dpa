package pathanalyzer;

import pathanalyzer.exceptions.PathException;

/**
 * Defines constants for the package (and app).
 */
public class AppConstants {

	//Indicates how long threads should wait on each other in nanoseconds
	public static final int threadSleepTime = 1;
	
	public static final Pattern matchFailed = new Pattern("NO MATCH");
	
	/**
	 * Used to allow other threads time to finish without spiking the CPU
	 * @throws InterruptedException
	 */
	public final static void sleep() throws InterruptedException {
		Thread.sleep(0, threadSleepTime);
	}

	public final static Pattern interruptPattern = new Pattern(new PathException("Interupted"));
}
