package pathanalyzer.exceptions;

public class PathException extends Exception {

	private static final long serialVersionUID = 623876407836446236L;

	public PathException(String msg) {
		super(msg);
	}
}
