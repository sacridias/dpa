package pathanalyzer.exceptions;

/**
 * Used to indicate a problem with threads or thread states
 *
 */
public class ThreadingException extends Exception {
	public ThreadingException(String string) {
		super(string);
	}

	private static final long serialVersionUID = 1687417189984707598L;
}
